class Character:
    def __init__(self,name,hp_points,attack_points):
        self.name = name
        self.hp_points = hp_points
        self.attack_points = attack_points


class Wrier(Character):
    def __init__(self, name, hp_points, attack_points,armor_points):
        super().__init__(name, hp_points, attack_points)
        self.armor_hp_points = armor_points


class Wizard(Character):
    def __init__(self, name, hp_points, attack_points,mana_points):
        super().__init__(name, hp_points, attack_points)
        self.mana_points = mana_points



class Enemy(Character):
    def __init__(self, name, hp_points, attack_points,resist_points):
        super().__init__(name, hp_points, attack_points)
        self.resist_points = resist_points


